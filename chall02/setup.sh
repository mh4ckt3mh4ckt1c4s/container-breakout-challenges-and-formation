#!/bin/bash

# initializing docker
curl -fsSL https://get.docker.com | bash
docker pull alpine:latest

# Setting files
mkdir /home/container02
mv /home/vagrant/flag02.txt /home/container02/flag02.txt
mv /home/vagrant/login.sh /home/container02/login.sh

# Creating the user

useradd container02 -s "/home/container02/login.sh" -p  $(echo "ct02SuperPassword" | openssl passwd -1 -stdin) -M -G docker

# Checking that the scripts and flags have good permissions

chown -R container02:container02 /home/container02
chmod 740 /home/container02/flag02.txt
chmod 750 /home/container02/login.sh

# SSH fixes
echo """
Banner none
AllowTcpForwarding no
X11Forwarding no
AllowStreamLocalForwarding no
"""  >> /etc/ssh/sshd_config
systemctl restart sshd
