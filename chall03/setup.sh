#!/bin/bash

# initializing docker
curl -fsSL https://get.docker.com | bash
docker pull alpine:latest

# Setting files
mkdir /home/container03
mv /home/vagrant/flag03.txt /home/container03/flag03.txt
mv /home/vagrant/login.sh /home/container03/login.sh

# Creating the user

useradd container03 -s "/home/container03/login.sh" -p  $(echo "ct03SuperPassword" | openssl passwd -1 -stdin) -M -G docker

# Checking that the scripts and flags have good permissions

chown -R container03:container03 /home/container03
chmod 740 /home/container03/flag03.txt
chmod 750 /home/container03/login.sh

# SSH fixes
echo """
Banner none
AllowTcpForwarding no
X11Forwarding no
AllowStreamLocalForwarding no
"""  >> /etc/ssh/sshd_config
systemctl restart sshd
