#!/bin/bash

# initializing docker
curl -fsSL https://get.docker.com | bash
docker pull alpine:latest

# Setting files
mkdir /home/container04
mv /home/vagrant/flag04.txt /home/container04/flag04.txt
mv /home/vagrant/login.sh /home/container04/login.sh

# Creating the user

useradd container04 -s "/home/container04/login.sh" -p  $(echo "ct04SuperPassword" | openssl passwd -1 -stdin) -M -G docker

# Checking that the scripts and flags have good permissions

chown -R container04:container04 /home/container04
chmod 740 /home/container04/flag04.txt
chmod 750 /home/container04/login.sh

# SSH fixes
echo """
Banner none
AllowTcpForwarding no
X11Forwarding no
AllowStreamLocalForwarding no
"""  >> /etc/ssh/sshd_config
systemctl restart sshd

# Launching server docker
docker run -dit --name apache -p 80:80 -v /:/usr/local/apache2/htdocs/ httpd:2.4
