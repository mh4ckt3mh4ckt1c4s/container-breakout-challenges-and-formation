# Container Breakout Challenges

## About

These challenges are container escape challenges, along with the slides used for the formation I gave to the [HackademINT](https://www.hackademint.org) club (in French).

Each player is connecting to a user and will be locked in its own container, which will be deleted on exit. Once rooted however, the user will have full access to the VM and will be able to interfere with other users (this setup is not suitable for a competitive CTF).

Each challenge have its own VM, allowing as much connections (and players) as needed.

## Setup

These challenges are managed by `vagrant`. You just have to clone this repository, go into each folder and run `vagrant up`. It may take a while to start.

Once started, each challenge will be available via ssh on ports 2222 to 2225 :

```bash
ssh container01@<server> -p 2222 (password : ct01SuperPassword)
ssh container02@<server> -p 2223 (password : ct02SuperPassword)
ssh container03@<server> -p 2224 (password : ct03SuperPassword)
ssh container04@<server> -p 2225 (password : ct04SuperPassword)
```

If needed, you can change the ssh ports in each `Vagrantfile` on line 31.

Once the challenges are done, you can delete the VMs by going into each directory and running `vagrant destroy`.
