#!/bin/bash

# initializing docker
curl -fsSL https://get.docker.com | bash
docker pull debian:11

# Setting files
mkdir /home/container01
mv /home/vagrant/flag01.txt /home/container01/flag01.txt
mv /home/vagrant/login.sh /home/container01/login.sh

# Creating the user

useradd container01 -s "/home/container01/login.sh" -p  $(echo "ct01SuperPassword" | openssl passwd -1 -stdin) -M -G docker

# Checking that the scripts and flags have good permissions

chown -R container01:container01 /home/container01
chmod 740 /home/container01/flag01.txt
chmod 750 /home/container01/login.sh

# SSH fixes
echo """
Banner none
AllowTcpForwarding no
X11Forwarding no
AllowStreamLocalForwarding no
"""  >> /etc/ssh/sshd_config
systemctl restart sshd
